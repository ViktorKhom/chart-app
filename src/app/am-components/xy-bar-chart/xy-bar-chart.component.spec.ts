import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XyBarChartComponent } from './xy-bar-chart.component';

describe('XyBarChartComponent', () => {
  let component: XyBarChartComponent;
  let fixture: ComponentFixture<XyBarChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XyBarChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XyBarChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
