import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-xy-mixed-chart',
  templateUrl: './xy-mixed-chart.component.html',
  styleUrls: ['./xy-mixed-chart.component.css']
})
export class XyMixedChartComponent implements OnInit {
  mixedChartData =  [{
                        "country": "Lithuania",
                        "litres": 501.9,
                        "units": 250
                    }, {
                        "country": "Czech Republic",
                        "litres": 301.9,
                        "units": 222
                    }, {
                        "country": "Ireland",
                        "litres": 201.1,
                        "units": 170
                    }, {
                        "country": "Germany",
                        "litres": 165.8,
                        "units": 122
                    }, {
                        "country": "Australia",
                        "litres": 139.9,
                        "units": 99
                    }, {
                        "country": "Austria",
                        "litres": 128.3,
                        "units": 85
                    }, {
                        "country": "UK",
                        "litres": 99,
                        "units": 93
                    }, {
                        "country": "Belgium",
                        "litres": 60,
                        "units": 50
                    }, {
                        "country": "The Netherlands",
                        "litres": 50,
                        "units": 42
                    }];
  constructor() { }

  ngOnInit(): void {
  }

}
