import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { XyMixedChartComponent } from './xy-mixed-chart.component';

describe('XyMixedChartComponent', () => {
  let component: XyMixedChartComponent;
  let fixture: ComponentFixture<XyMixedChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ XyMixedChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(XyMixedChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
