import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AmRadarChartComponent } from './am-radar-chart.component';

describe('AmRadarChartComponent', () => {
  let component: AmRadarChartComponent;
  let fixture: ComponentFixture<AmRadarChartComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmRadarChartComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmRadarChartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
