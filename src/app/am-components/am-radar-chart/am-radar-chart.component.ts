import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-am-radar-chart',
  templateUrl: './am-radar-chart.component.html',
  styleUrls: ['./am-radar-chart.component.css']
})
export class AmRadarChartComponent implements OnInit {
  radarChartData =  [{
      "country": "Lithuania",
      "litres": 501.9
    }, {
      "country": "Czech Republic",
      "litres": 301.9
    }, {
      "country": "Ireland",
      "litres": 201.1
    }, {
      "country": "Germany",
      "litres": 165.8
    }, {
      "country": "Australia",
      "litres": 139.9
    }, {
      "country": "Austria",
      "litres": 128.3
    }, {
      "country": "UK",
      "litres": 99
    }, {
      "country": "Belgium",
      "litres": 60
    }, {
      "country": "The Netherlands",
      "litres": 50
    }];
    
  constructor() { }

  ngOnInit(): void {
  }

}
