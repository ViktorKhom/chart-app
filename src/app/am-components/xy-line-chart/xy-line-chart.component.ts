import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-xy-line-chart',
  templateUrl: './xy-line-chart.component.html',
  styleUrls: ['./xy-line-chart.component.css']
})
export class XyLineChartComponent implements OnInit {
  lineChartData = [{
                              "country": "Lithuania",
                              "litres": 501
                          }, {
                              "country": "Czechia",
                              "litres": 301
                          }, {
                              "country": "Ireland",
                              "litres": 201
                          }, {
                              "country": "Germany",
                              "litres": 165
                          }, {
                              "country": "Australia",
                              "litres": 139
                          }, {
                              "country": "Austria",
                              "litres": 128
                          }, {
                              "country": "UK",
                              "litres": 99
                          }, {
                              "country": "Belgium",
                              "litres": 60
                          }, {
                              "country": "The Netherlands",
                              "litres": 50
                          }];
  constructor() { }

  ngOnInit(): void {
  }

}
