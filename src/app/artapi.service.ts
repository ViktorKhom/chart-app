import { Injectable } from '@angular/core';
import { environment } from '../environments/environment';
import { HttpClient, HttpParams, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError, of } from 'rxjs';
import { tap, catchError, retry, switchMap, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ArtapiService {

  private servinfo = {};
  
  /* move to enviroments */
  //url: string = '/node/mobile/';
	//url_public: string = '/node/public/';

  constructor(private http: HttpClient) {}

  private formatErrors(error: any) {
    return throwError(error.error);
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(`Backend returned code ${error.status}, body was: ${error.message}`);
    }
    return throwError('Something bad happened; please try again later.');
  };

  /*
  get(path: string, params: HttpParams = new HttpParams()): Observable<any> {
    return this.http.get(`${environment.url}${path}`, { params })
      .pipe(catchError(this.formatErrors));
  }

  put(path: string, body: Object = {}): Observable<any> {
    return this.http.put(
      `${environment.url}${path}`,
      JSON.stringify(body)
    ).pipe(catchError(this.formatErrors));
  }

  post(path: string, body: Object = {}): Observable<any> {
    return this.http.post(
      `${environment.url}${path}`,
      JSON.stringify(body)
    ).pipe(catchError(this.formatErrors));
  }

  delete(path): Observable<any> {
    return this.http.delete(
      `${environment.url}${path}`
    ).pipe(catchError(this.formatErrors));
  }*/

  round(value: string | number, decimals: number | string = "0"): number | null {
    return +( Math.round(Number(value + "e+"+decimals)) + "e-" + decimals);
  }

  // function _round(number, precision) {
  //   var pair = (number + 'e').split('e');
  //   var value = Math.round(pair[0] + 'e' + (+pair[1] + precision));
  //   pair = (value + 'e').split('e');
  //   return +(pair[0] + 'e' + (+pair[1] - precision));
  // }

  sqlService(sqlServId: string, params: any = {}, publicmark: boolean = false, method: string = 'post', config: any = {}, securityCustomHandlerMark: boolean = false): Observable<any> {
    //const url = (publicmark ? environment.url_public : environment.url);
    const methodFn = (method.toLowerCase() === 'get' ? this.get : this.post0).bind(this);
    const body = {
      action: 'api.mobile.sqlService',
      args:{
        servid: sqlServId,
        params: params
      }
    }
    return methodFn(body, config, publicmark).pipe(
      //catchError(this.formatErrors),
      catchError(this.handleError),
      //tap(res => console.log(res)),
      map((res: any) => {
        console.log('--res',res);
        if (res.status == 1) {
          if (res.data.hasOwnProperty('outBinds')) {
            return [res.data];
          } else if (res.data.hasOwnProperty('rows')) {
            const metaData = res.data.metaData;
            const rows = res.data.rows;
            let result = [];
            for (var i = 0; i < rows.length; i++) {
              let data = {};
              for (var md in metaData ) {
                if (metaData[md].fetchType === 2002 && metaData[md].scale > 0) {
                  data[metaData[md].name] = this.round(rows[i][md], metaData[md].scale);
                } else {
                  data[metaData[md].name] = rows[i][md];
                }
              }
              result.push(data);
            }
            return result;
          } else {
            return [];
          }
        } else if (res.status === 3) {
          //2factor
          if (res.$code) console.log("SMS code: "+res.$code);
          const {idcomponent, key} = res.data;
          if (idcomponent && securityCustomHandlerMark) return null;//self.msgboxWithCallback(idcomponent, Object.assign({}, params, response.data.data), '');
          else return [{outBinds: Object.assign({}, params, res.data)}];
        } else {
          console.error("Извлечение данных SQL сервисом "+sqlServId+" с параметрами: ", params);
			    console.error("завершилось с ошибкой:", res.description);
          //throwError('Something bad happened; please try again later.');
        }
      })
    );

    //return request(body, config, publicmark).pipe(
    //  tap(res => console.log('--res',res))
    //);
    //return this.http.post(url, body).pipe(
    //);
    //this.http[method]((publicmark ? url_public : url ), params)
  }

  post0(data: any, config: any = {}, publicmark: boolean = false): Observable<any> {
    return this.getServInfo(data.args.servid).pipe(
      switchMap(res => {
        console.log('--post0 switchMap', res);
        if (res.cachemark) {
          return this.get(data, config, publicmark);
        } else {
          return this.post(data, config, publicmark);
        }
      })
    )
  }

  post(data: any, config: any = {}, publicmark: boolean = false): Observable<any> {
    const url = (publicmark ? environment.url_public : environment.url);
    return this.http.post(url, data, config);
  }

  get(data: any, config: any = {}, publicmark: boolean = false): Observable<any> {
    const url = (publicmark ? environment.url_public : environment.url) + 'sqlService/' + data.args.servid;
    return this.http.get(url, {params: data})
  }

  getServInfo(servid: string): Observable<any> {
    if (!this.servinfo[servid]) {
      return this.http.get(`${environment.url_public}sqlService/sys_get_serv_info`, {params: {servid}}).pipe(
        map((res: any) => {
          console.log('--map',res);
          const v = res.data.rows[0];
          if (v) this.servinfo[v[0]]={cachemark:v[1],publicmark:v[2]};
          return this.servinfo[servid];
        })
      )
    } else {
      return of(this.servinfo[servid]);
    }
  }

  request(method: string, url: string, data: any = {}, config: any = {}) {
    if (method && url) {
      return this.http.request(method, url, Object.assign(config, {body: data})).pipe(catchError(this.handleError));
    }
  }

  updService(servid: string, action: string, params: any): Observable<any> {
    const replacer = (key, value) => typeof value === 'undefined' ? null : value;
    const data = {
      action: "api.mobile.updService",
      args: {
        servid: servid,
        action: action,
        data: params
      }
    }
    return this.post(JSON.stringify(data, replacer)).pipe(
      catchError(this.handleError),
      map((res: any) => ({data: res})) 
      //map - для совмесимости с angularjs. Если использовать {observe: 'response'}, то вместо data будет body
    )
  }
}
