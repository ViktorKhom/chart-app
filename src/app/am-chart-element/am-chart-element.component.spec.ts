import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AmChartElementComponent } from './am-chart-element.component';

describe('AmChartElementComponent', () => {
  let component: AmChartElementComponent;
  let fixture: ComponentFixture<AmChartElementComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AmChartElementComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AmChartElementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
