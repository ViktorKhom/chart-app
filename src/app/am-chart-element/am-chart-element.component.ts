import { Component, OnInit, Input, NgZone, AfterViewInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ArtapiService } from '../artapi.service';
import * as am4core from "@amcharts/amcharts4/core";
import * as am4charts from "@amcharts/amcharts4/charts";
import * as am4maps from "@amcharts/amcharts4/maps";
import am4geodata_worldLow from "@amcharts/amcharts4-geodata/worldLow";

@Component({
  selector: 'app-am-chart-element',
  templateUrl: './am-chart-element.component.html',
  styleUrls: ['./am-chart-element.component.css']
})
export class AmChartElementComponent implements OnInit, AfterViewInit {

  private baseUrl: string = '/node/mobile/sqlService/';
  @Input() chartId:any = '12412';
  @Input() childData:any;
  @Input() chartType:string = 'PieChart';
  @Input() chartTypeSeries:string = 'Line';
  @Input() chartValueX:string = 'ValueX';
  @Input() chartValueY:string = 'ValueY';
  @Input() chartValueY1:string = 'ValueY1';
  @Input() chartData: any[];
  @Input() sqlservid:string = 'cnt_create_close_cards_list';
  @Input() sqlservparams:{} = {};
  @Input() sqlservmapnames:any = [];
  @Input() sqlservresultpath:string = '';
  @Input() chartLabels = ['01', '02', '03', '04', '05', '06'];
  @Input() lineColor = '#CDA2AB';
  @Input() columnColor = '#104547';
  @Input() lineStrokeWidth = 3;
  @Input() chartLegend:boolean = true;

  constructor(private zone: NgZone, private http: HttpClient) {}

  ngOnInit(): void {
    this.http.get(this.baseUrl + this.sqlservid, {params: this.sqlservparams}).subscribe((result: any) => {
      if(result.data.outBinds.p_card_arr.length > 0) {
        let allLables = this.arrayToObject(result.data.outBinds.p_card_arr, this.sqlservmapnames);
        this.chartLabels = allLables[this.sqlservmapnames];
      }
    });
  }

  ngAfterViewInit() {
    this.zone.runOutsideAngular(() => {
      this['create' + this.chartType]();
      console.log('create' + this.chartType);
    });
  }

  arrayToObject(arr, mapnames) {
    var rslt = {};
    for(let i = 0; i < arr.length; i++) {
      for (let key in arr[i]) {
        let k = mapnames[key] || key;
        if (i === 0) {rslt[k] = []}
        rslt[k].push(arr[i][key]);
      }
    }
		return rslt;
  }

  createXYChart() {
    let chart = am4core.create(this.chartId, am4charts.XYChart);
    chart.data = this.chartData;
    let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = this.chartValueX;
    categoryAxis.title.text = this.chartValueX;
    
    let valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    valueAxis.title.text = this.chartValueY;
    this['createXYChart' + this.chartTypeSeries + 'Series'](chart);
  }

  createXYChartMixedSeries(chart) {
    let series = chart.series.push(new am4charts.ColumnSeries());
    series.dataFields.valueY = this.chartValueY1;
    series.dataFields.categoryX = this.chartValueX;
    series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
    series.columns.template.fill = am4core.color(this.columnColor);

    let series2 = chart.series.push(new am4charts.LineSeries());
    series2.dataFields.valueY = this.chartValueY;
    series2.dataFields.categoryX = this.chartValueX;
    series2.tooltipText = "{categoryX}: [bold]{valueY}[/]";
    series2.stroke = am4core.color(this.lineColor);
    series2.strokeWidth = this.lineStrokeWidth;
    chart.cursor = new am4charts.XYCursor();
  }

  createXYChartColumnSeries(chart) {
    let series = chart.series.push(new am4charts[this.chartTypeSeries + 'Series']());
    series.dataFields.valueY = this.chartValueY;
    series.dataFields.categoryX = this.chartValueX;
    series.columns.template.tooltipText = "{categoryX}: [bold]{valueY}[/]";
    series.columns.template.fill = am4core.color("#104547");
    chart.cursor = new am4charts.XYCursor();
    chart.legend = new am4charts.Legend();
    chart.legend.position = 'top';
    series.legendSettings.labelText = this.chartValueX;
  }

  createXYChartLineSeries(chart) {
    let series = chart.series.push(new am4charts[this.chartTypeSeries + 'Series']());
    series.stroke = am4core.color("#CDA2AB");
    series.strokeWidth = 3;
    series.dataFields.valueY = this.chartValueY;
    series.dataFields.categoryX = this.chartValueX;
    series.tooltipText = "{categoryX}: [bold]{valueY}[/]";
    chart.cursor = new am4charts.XYCursor();
    chart.legend = new am4charts.Legend();
    chart.legend.position = 'top';
    series.legendSettings.labelText = this.chartValueX;
  }

  createPieChart() {
    let chart = am4core.create(this.chartId, am4charts.PieChart3D);
    chart.data = this.chartData;
    let pieSeries = chart.series.push(new am4charts.PieSeries3D());
    pieSeries.dataFields.value = this.chartValueY;
    pieSeries.dataFields.category = this.chartValueX;
  }

  createRadarChart() {
    let chart = am4core.create(this.chartId, am4charts.RadarChart);
    let categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    categoryAxis.dataFields.category = this.chartValueX;
    var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    let series = chart.series.push(new am4charts.RadarSeries());
    chart.data = this.chartData;
    series.dataFields.valueY = this.chartValueY;
    series.dataFields.categoryX = this.chartValueX;
  }

}
