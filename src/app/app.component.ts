import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ArtapiService } from './artapi.service';
import { BehaviorSubject, EMPTY } from 'rxjs';
import { catchError, shareReplay, tap } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  private url: string = '/node/mobile';
  private loggedIn = new BehaviorSubject<boolean>(false);
  public user$ = new BehaviorSubject<string>(null);

  constructor(private http: HttpClient, private artapi: ArtapiService) { }

  ngOnInit() {
    if(localStorage.getItem("atoken") == 'null') {
      this.auth();
    }
  }

  auth() {
    return this.login('khomenko', 'vkhom_baza10').subscribe(() => {
     console.log('User Logged In');
    });
  }

  login(login: string, password: string) {
    return this.http.post<any>(this.url + '/auth', {login, pass: password}, {observe: 'response'}).pipe(
      catchError(error => {
        console.error(error);
        return EMPTY;
      }),
      shareReplay(),
      tap(res => {
        this.setSession(res);
        this.loggedIn.next(true);
        this.user$.next(login);
        //this.router.navigate(['/']);
      })
    )
  }

  private setSession(res) {
    console.log('--setSession', res);
    localStorage.setItem('atoken', res.headers.get('Authorization'));
  }

}
