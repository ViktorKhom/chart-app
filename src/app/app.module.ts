import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { ChartsModule } from 'ng2-charts';
import { HTTP_INTERCEPTORS  } from '@angular/common/http';
import { AppComponent } from './app.component';
import { BarChartComponent } from './components/bar-chart/bar-chart.component';
import { BubbleChartComponent } from './components/bubble-chart/bubble-chart.component';
import { ChartElementComponent } from './chart-element/chart-element.component';
import { DoughnutChartComponent } from './components/doughnut-chart/doughnut-chart.component';
import { LineChartComponent } from './components/line-chart/line-chart.component';
import { PieChartComponent } from './components/pie-chart/pie-chart.component';
import { RadarChartComponent } from './components/radar-chart/radar-chart.component';
import { ScatterChartComponent } from './components/scatter-chart/scatter-chart.component';

import { AuthInterceptor } from './auth.interceptor';
import { AmChartElementComponent } from './am-chart-element/am-chart-element.component';
import { XyLineChartComponent } from './am-components/xy-line-chart/xy-line-chart.component';
import { XyBarChartComponent } from './am-components/xy-bar-chart/xy-bar-chart.component';
import { XyMixedChartComponent } from './am-components/xy-mixed-chart/xy-mixed-chart.component';
import { AmPieChartComponent } from './am-components/am-pie-chart/am-pie-chart.component';
import { AmRadarChartComponent } from './am-components/am-radar-chart/am-radar-chart.component';

@NgModule({
  declarations: [
    AppComponent,
    BarChartComponent,
    BubbleChartComponent,
    ChartElementComponent,
    DoughnutChartComponent,
    LineChartComponent,
    PieChartComponent,
    RadarChartComponent,
    ScatterChartComponent,
    AmChartElementComponent,
    XyLineChartComponent,
    XyBarChartComponent,
    XyMixedChartComponent,
    AmPieChartComponent,
    AmRadarChartComponent
  ],
  imports: [
    BrowserModule,
    ChartsModule,
    HttpClientModule
  ],
  providers: [{ provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true }],
  bootstrap: [AppComponent]
})
export class AppModule { }
