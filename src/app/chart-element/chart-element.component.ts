import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { ArtapiService } from '../artapi.service';
import { ChartDataSets, ChartOptions } from 'chart.js';
import { Color, Label } from 'ng2-charts';

@Component({
  selector: 'app-chart-element',
  templateUrl: './chart-element.component.html',
  styleUrls: ['./chart-element.component.css']
})
export class ChartElementComponent implements OnInit {
  private baseUrl: string = '/node/mobile/sqlService/';
  @Input() childData:any;
  @Input() chartType:string = 'bar';
  @Input() chartData:ChartDataSets[] = [
    { data: [510, 590, 1850, 730, 700, 530], label: 'Profit' },
  ];
  @Input() sqlservid:string = 'cnt_create_close_cards_list';
  @Input() sqlservparams:{} = {};
  @Input() sqlservmapnames:any = [];
  @Input() sqlservresultpath:string = '';
  @Input() chartLabels:Label[] = ['01', '02', '03', '04', '05', '06'];
  @Input() chartOptions:ChartOptions = {
    responsive: true,
  };
  @Input() chartColors:Color[] =  [
    {
      borderColor: 'rgba(132,171,0,1.0)',
      backgroundColor: 'rgba(237,116,0,0.5)',
    },
  ];
  @Input() chartLegend:boolean = true;
  @Input() chartPlugins:[] = [];
  @Input() interval:any;

  constructor(private artapi: ArtapiService, private http: HttpClient) { }

  ngOnInit(): void {
    // this.artapi.sqlService(this.sqlservid,this.sqlservparams,null,'get',{cache: true}).subscribe(function(data) {
    //   console.log(data);
    // });
    if(this.childData) {
      for(let i = 0; i < this.childData.length; i++) {
        this.getChildLabels(this.childData[i].sqlservid, this.childData[i].sqlservparams, this.childData[i].sqlservmapnames, i);
      }
    }
    this.http.get(this.baseUrl + this.sqlservid, {params: this.sqlservparams}).subscribe((result: any) => {
      if(result.data.outBinds.p_card_arr.length > 0) {
        let allLables = this.arrayToObject(result.data.outBinds.p_card_arr, this.sqlservmapnames);
        this.chartLabels = allLables[this.sqlservmapnames];
      }
    });
  }

  getChildLabels(sqlservid, sqlservparams, mapnames, childIndex) {
    this.http.get(this.baseUrl + sqlservid, {params: sqlservparams}).subscribe((result: any) => {
      if(result.data.outBinds.p_card_arr.length > 0) {
        if(this.chartData[childIndex]) {
          this.chartData[childIndex] = {data: this.arrayToObject(result.data.outBinds.p_card_arr, mapnames)[mapnames], label: this.childData[childIndex]['label']};
        } else {
          this.chartData.push({data: this.arrayToObject(result.data.outBinds.p_card_arr, mapnames)[mapnames], label: this.childData[childIndex]['label']});
        }
        if(this.chartColors[childIndex]) {
          this.chartColors[childIndex] = {'borderColor': this.childData[childIndex]['background-color'], backgroundColor: this.childData[childIndex]['background-color'] }};
        } else {
          this.chartColors.push({'borderColor': this.childData[childIndex]['border-color'], backgroundColor: this.childData[childIndex]['background-color'] });
        }
    });
  }

  arrayToObject(arr, mapnames) {
    var rslt = {};
    for(let i = 0; i < arr.length; i++) {
      for (let key in arr[i]) {
        let k = mapnames[key] || key;
        if (i === 0) {rslt[k] = []}
        rslt[k].push(arr[i][key]);
      }
    }
		return rslt;
  }

}
