import { Component, OnInit } from '@angular/core';
import { ChartService } from '../../services/chart.service';
@Component({
  selector: 'app-line-chart',
  templateUrl: './line-chart.component.html',
  styleUrls: ['./line-chart.component.css']
})
export class LineChartComponent implements OnInit {

  constructor(private chartServ: ChartService) { }

  ngOnInit(): void {
    this.chartServ.getChartData().subscribe((result:any) => {
      console.log(result);
    });
  }

}
